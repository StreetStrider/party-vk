/* @flow */

; type UserData =
{
	recent_name:  string,
	recent_image: string,
}

; export type User = UserData &
{
	vk_id: number,
}

; export type Leader =
{
	is_leader: boolean,
}

;

var assign = Object.assign

import Table  from 'knexed/table/table'
import method from 'knexed/tx/method'
import exists from 'knexed/exists'
import one    from 'knexed/one'

export default function UserVk (app: Object)
{
	var pg = app.db.pg
	var leader_id = app.cfg.party.leader.id

	var u = {}

	var kx = pg.kx
	var table = Table(kx, 'user_vk')

	u.update = method(kx, (tx, vk_id: number, data: UserData) =>
	{
		/* @flow-off */
		var data_compose = assign({ vk_id }, data)

		return byId(tx, vk_id)
		.select()
		.then(exists)
		.then(so =>
		{
			if (so)
			{
				return byId(tx, vk_id)
				.update(data)
			}
			else
			{
				return table(tx)
				.insert(data_compose)
			}
		})
		/* @flow-off */
		.then((): User => data_compose)
		.then(apply_leader)
	})

	u.byId = (vk_id: number) =>
	{
		return byId(null, vk_id)
		.then(one)
		.then(apply_leader)
	}

	var byId = (tx, vk_id: number) =>
	{
		return table(tx).where('vk_id', vk_id)
	}

	var apply_leader = (data: User): User & Leader =>
	{
		/* @flow-off */
		data.is_leader = (data.vk_id === leader_id)

		/* @flow-off */
		return (data: User & Leader)
	}

	return u
}
