/* @flow */

export type Data =
{
	will_go: boolean,
	drink:   string,
}

;

import map from 'lodash/map'
import includes from 'lodash/includes'

import Table  from 'knexed/table/table'
import Join   from 'knexed/table/join'
import method from 'knexed/tx/method'
import exists from 'knexed/exists'

import Wrong from 'express-toss/dist/Wrong'

var FieldRequired = Wrong('field_required')
var WrongDrink = Wrong('wrong_drink')


var drinks_available =
[
	{ id: 'tea',  name: 'Tea'  },
	{ id: 'cola', name: 'Cola' },
	{ id: 'beer', name: 'Beer' },
]
var drinks_available_id = map(drinks_available, 'id')


export default function Poll (pg: Object)
{
	var poll = {}

	var kx = pg.kx
	var table = Table(kx, 'poll')

	var table_with_userinfo = Join(table, Table(kx, 'user_vk'), 'vk_id')

	poll.drinks = () => drinks_available

	poll.answered = (vk_id: number) =>
	{
		return byId(null, vk_id)
		.then(exists)
	}

	poll.create = method(kx, (tx, vk_id, data: Data) =>
	{
		if (! ('will_go' in data))
		{
			throw FieldRequired({ field: 'will_go' })
		}
		if (! ('drink' in data))
		{
			throw FieldRequired({ field: 'drink' })
		}
		if (! includes(drinks_available_id, data.drink))
		{
			throw WrongDrink(
			{
				actual: data.drink,
				desired: drinks_available_id
			})
		}

		var data_full =
		{
			vk_id,
			will_go: Boolean(data.will_go),
			drink: data.drink
		}

		return erase(tx, vk_id)
		.then(() =>
		{
			return table(tx)
			.insert(data_full)
		})
		.then(() => data_full)
	})

	function erase (tx, vk_id: number)
	{
		return byId(tx, vk_id)
		.delete()
	}

	function byId (tx, vk_id: number)
	{
		return table(tx)
		.where('vk_id', vk_id)
	}

	poll.list = () =>
	{
		return table_with_userinfo().select()
	}

	return poll
}
