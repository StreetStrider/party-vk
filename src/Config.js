/* @flow */

; import type { T_Rootpath as Rootpath } from 'rootpath'

import { readJSON as load, exists } from 'fs-sync'
import { merge } from 'lodash'

export default function (root: Rootpath)
{
	var cfg = load(root('cfg.json'))
	var dev = root('dev.json')

	if (exists(dev))
	{
		cfg = merge({}, cfg, load(dev))
	}

	return cfg
}
