/* @flow */

type Options =
{
	from: string,
	to:   string,
	subject: string,

	text?: string,
	html?: string,
}

;

import assert from 'assert'

import Mailer from 'nodemailer'

export default function Mail (cfg: Object)
{
	assert(cfg.auth, 'smtp auth must be present')
	assert(cfg.auth.user, 'user must be present')
	assert(cfg.auth.pass, 'pass must be present')

	var transport = Mailer.createTransport(cfg)

	var m = {}

	m.send = (options: Options) =>
	{
		return new Promise((rs, rj) =>
		{
			return transport.sendMail(options, (error, result) =>
			{
				if (error)
				{
					return rj(error)
				}
				else
				{
					return rs(result)
				}
			})
		})
	}

	return m
}
