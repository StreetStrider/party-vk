/* @flow */
/* global express$Request */

type express$Request$Auth = express$Request &
{
	user: Object
}

;

import pick from 'lodash/pick'

import Router from 'express/lib/router'
import Body from 'body-parser'

export default function Ctrl (app: Object)
{
	var http = app.http

	var express = http.express

	var auth_only   = http.auth_only
	var leader_only = http.leader_only

	var method = http.tosser.method

	{
		var poll = Router()
		poll.use(auth_only)
		poll.use(Body.json())

		poll.get('/', method((rq: express$Request$Auth) =>
		{
			var vk_id = rq.user.vk_id

			return app.poll.answered(vk_id)
			.then((so) =>
			{
				return {
					answered: so,
					drinks: app.poll.drinks(),
				}
			})
		}))

		poll.post('/', method((rq: express$Request$Auth) =>
		{
			var user = rq.user
			var data = pick(rq.body, [ 'will_go', 'drink' ])

			return app.poll_and_notify(user, data)
		}))

		/* leader route */
		poll.get('/all', leader_only, method(() =>
		{
			return app.poll.list()
		}))
	}

	express.use('/poll', poll)

	express.get('/user', auth_only, (rq: express$Request, rs) =>
	{
		/* @flow-off */
		rs.send({ user: rq.user })
	})
}
