/* @flow */

import Pg from './Pg'
import Redis from './Redis'

export default function Db (cfg: Object)
{
	var db = {}

	db.pg = Pg(cfg.pg)
	db.redis = Redis(cfg.redis)

	db.ready = Promise.all([ db.pg.ready, db.redis.ready ])

	return db
}
