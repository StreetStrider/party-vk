/* @flow */

import Knex from 'knex'

export default function Pg (cfg: Object)
{
	var pg = {}

	var kx = pg.kx = Knex(
	{
		client: 'pg',
		connection: cfg,
		pool:
		{
			min: 2,
			max: 10
		},
	})

	pg.ready = Promise.resolve()
	.then(() =>
	{
		return kx.select(kx.raw('current_database()'))
	})

	return pg
}
