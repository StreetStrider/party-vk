/* @flow */

import redis from 'ioredis'

export default function Redis (cfg: Object)
{
	var rd = {}

	rd.ioredis = redis(cfg)

	rd.ready = rd.ioredis.ping()

	return rd
}
