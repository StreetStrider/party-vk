/* @flow */
/* global express$Request */
/* global express$Response */
/* global express$NextFunction */

import Express from 'express'

import Cookie from 'cookie-parser'
import Session from 'express-session'

import ConnectRedis from 'connect-redis'
var RedisStore = ConnectRedis(Session)

import Tosser from 'express-toss/dist/tosser'

import Passport from './Passport'

export default function (app: Object)
{
	var http = {}

	var express = http.express = Express()

	if (app.cfg.dev)
	{
		express.use(Express.static(app.root('web/')))
	}

	express.use(Cookie())
	express.use(Session(
	{
		store: new RedisStore({ client: app.db.redis.ioredis }),
		secret: 'lZzXYs5oe850YQ1JAJjoclIoWpjdXDgX',
		resave: false,
		saveUninitialized: false,
	}))

	http.passport = Passport(app, express)

	var port = app.cfg.host.port
	http.ready = new Promise(rs =>
	{
		express.listen(port, rs)
	})
	.then(() =>
	{
		console.info(`HTTP at :${port}`)
	})

	http.tosser = Tosser({ debug: app.cfg.dev })

	http.auth_only = function
	(
		rq: express$Request,
		rs: express$Response,
		next: express$NextFunction
	)
	{
		if (rq.user)
		{
			next()
		}
		else
		{
			rs.status(401).end()
		}
	}

	http.leader_only = function
	(
		rq: express$Request,
		rs: express$Response,
		next: express$NextFunction
	)
	{
		if (rq.user && rq.user.vk_id === app.cfg.party.leader.id)
		{
			next()
		}
		else
		{
			rs.status(401).end()
		}
	}

	return http
}
