/* @flow */
/* global express$Application */
/* global express$Request */

import passport from 'passport'

import Vk from './Vk'

export default function Passport (app: Object, express: express$Application)
{
	passport.serializeUser((user, done) =>
	{
		return done(null, user.vk_id)
	})

	passport.deserializeUser((id, done) =>
	{
		return app.users.byId(id)
		.then(u => done(null, u), done)
	})

	express.use(passport.initialize())
	express.use(passport.session())

	// LocalBuddy(express, passport)
	Vk(express, passport, app)

	express.delete('/login', (rq: express$Request, rs) =>
	{
		/* @flow-off */
		rq.logout()
		rs.end()
	})
}
