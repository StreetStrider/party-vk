/* @flow */
/* global express$Application */

import url from 'url'
import assert from 'assert'

import { Strategy as Vk } from 'passport-vkontakte'

var callback = '/login/vk/callback'

export default function
(
	express: express$Application,
	passport: any,
	app: Object
)
{
	var cfg   = app.cfg
	var users = app.users

	assert(cfg.vk.id, 'cfg.vk.id must be present')
	assert(cfg.vk.secret, 'cfg.vk.secret must be present')

	var callback_uri = url.format(
	{
		protocol: 'http',
		hostname: cfg.host.hostname,
		port: cfg.host.port,
		pathname: callback,
	})

	passport.use(new Vk(
	{
		clientID: cfg.vk.id,
		clientSecret: cfg.vk.secret,
		callbackURL: callback_uri,
	},
	// eslint-disable-next-line max-params
	(accessToken, refreshToken, params, profile, done) =>
	{
		var vk_id = profile.id
		var data =
		{
			recent_name:  profile.displayName,
			recent_image: profile.photos[0].value
		}

		return users.update(vk_id, data)
		.then(u => done(null, u), done)
	}))

	express.get('/login/vk', passport.authenticate('vkontakte'))
	express.get(callback, passport.authenticate('vkontakte',
	{
		successRedirect: '/',
		failureRedirect: '/',
	}))
}
