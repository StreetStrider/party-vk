/* @flow */
/* global express$Application */

import Local from 'passport-local'

export var buddy =
{
	id: 1,
	username: 'test',
	password: 'secret',
}

export function LocalBuddy
(
	express: express$Application,
	passport: any
)
{
	passport.use(new Local(
	{
		usernameField: 'username',
		passwordField: 'password',
	},
	(username, password, done) =>
	{
		if (username === buddy.username && password === buddy.password)
		{
			done(null, buddy)
		}
		else
		{
			done(null, false, { message: 'auth_error' })
		}
	}))

	express.post('/login', passport.authenticate('local',
	{
		successRedirect: '/user',
	}))
}
