/* @flow */

var noop = () => {}

import type { User } from './model/UserVk'
import type { Data as PollData } from './model/Poll'

import Rootpath from 'rootpath'

import Config from './Config'
import Http   from './Http'
import Db     from './Db'
import Mail   from './Mail'

import UserVk from './model/UserVk'
import Poll   from './model/Poll'

import Ctrl from './Ctrl'

export default function ()
{
	var app = {}

	app.root = Rootpath(__dirname, '..')
	console.info('package at `%s`', app.root())

	app.cfg   = Config(app.root.partial('cfg'))
	app.db    = Db(app.cfg.db)
	app.mail  = Mail(app.cfg.smtp)

	app.users = UserVk(app)
	app.poll  = Poll(app.db.pg)

	app.http  = Http(app)

	app.poll_and_notify = (user: User, data: PollData) =>
	{
		return app.poll.create(user.vk_id, data)
		.then(it =>
		{
			app.mail.send(
			{
				to: app.cfg.party.leader.email,
				from: 'solitaire.medici@gmail.com',
				subject: 'Poll changed',
				text: `${ user.recent_name } changed his intention on party event`,
			})
			.then(noop, noop)

			return it
		})
	}

	Ctrl(app)

	app.ready = Promise.all(
	[
		app.http.ready,
		app.db.ready,
	])

	app.ready.then(() =>
	{
		console.info('UP')
	}
	, console.error)

	return app
}
