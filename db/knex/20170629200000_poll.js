/* @flow */

export function up (kx: any)
{
	var sql = kx.raw

	// ~ CREATE DOMAIN drink_value ... CHECK(drinks)

	return sql(
`
CREATE TABLE poll
(
	vk_id   INTEGER NOT NULL PRIMARY KEY,

	will_go BOOL    NOT NULL,
	drink   VARCHAR NOT NULL,

	FOREIGN KEY (vk_id) REFERENCES user_vk(vk_id)
		ON UPDATE CASCADE
		ON DELETE CASCADE
)`)
}

export function down (kx: any)
{
	var sql = kx.raw

	return sql('DROP TABLE poll')
}
