/* @flow */

export function up (kx: any)
{
	var sql = kx.raw

	return sql(
`
CREATE TABLE user_vk
(
	vk_id INTEGER NOT NULL PRIMARY KEY,

	recent_name  VARCHAR NOT NULL,
	recent_image VARCHAR NOT NULL
)`)
}

export function down (kx: any)
{
	var sql = kx.raw

	return sql('DROP TABLE user_vk')
}
