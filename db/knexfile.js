/* @flow */

import Rootpath from 'rootpath'
import Config from '../src/Config'

export default
{
	development:
	{
		client: 'pg',
		connection: Config(Rootpath(__dirname, '../cfg')).db.pg,

		migrations:
		{
			tableName: 'knex',
			directory: './knex'
		}
	}
}
