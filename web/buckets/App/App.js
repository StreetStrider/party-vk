/* @flow */

export default
{
	template: '<div></div>',

	inject: [ 'auth' ],

	created ()
	{
		this.$http.get('/user')
		.then(it =>
		{
			this.auth.userinfo = it.body.user

			this.$router.push('/form')
		},
		() =>
		{
			/* go external */
			location = '/login/vk'
		})
	}
}
