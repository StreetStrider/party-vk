/* @flow */

import auth_only from 'index/auth-only'

import template from './Form.static.pug'

export default
{
	template,

	mixins: [ auth_only ],

	data ()
	{
		var data =
		{
			list: [],
			poll:  null,
			form:
			{
				will_go: null,
				drink:   null,
			},
			error:
			{
				is: false,
				fields:
				{
					will_go: false,
					drink:   false,
				},
			},
		}

		return data
	},

	filters:
	{
		yesno: (it) => it && 'yes' || 'no',
		tea: (it) => it[0].toUpperCase() + it.slice(1),
	},

	created ()
	{
		var userinfo = this.auth.userinfo

		if (userinfo && userinfo.is_leader)
		{
			this.$http.get('/poll/all')
			.then(it =>
			{
				this.list = it.body
			})
		}
		if (userinfo && ! userinfo.is_leader)
		{
			this.$http.get('/poll')
			.then(it =>
			{
				this.poll = it.body
			})
		}
	},

	methods:
	{
		bg (person)
		{
			return { backgroundImage: `url(${ person.recent_image })` }
		},

		errored (field)
		{
			return !! this.error.fields[field]
		},

		revote ()
		{
			this.poll.answered = false
		},

		logout ()
		{
			this.$http.delete('/login')
			location = '/'
		},

		submit ()
		{
			var ensure_filled = (field) =>
			{
				if (this.form[field] == null)
				{
					this.error.is = true
					this.error.fields[field] = true
				}
				else
				{
					this.error.fields[field] = false
				}
			}

			this.error.is = false

			ensure_filled('will_go')
			ensure_filled('drink')

			if (! this.error.is)
			{
				this.poll.answered = true

				this.$http.post('/poll', this.form)
			}
		},
	}
}
