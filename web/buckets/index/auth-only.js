
export default
{
	inject: [ 'auth' ],

	beforeRouteEnter (to, from, next)
	{
		next(vm =>
		{
			if (vm.auth && vm.auth.userinfo)
			{
				next()
			}
			else
			{
				next('/')
			}
		})
	},
}
