/* @flow */

/* @flow-off */
import Vue from 'vue/dist/vue.esm.js'
/* @flow-off */
import Router from 'vue-router'

Vue.use(Router)


import Resource from 'vue-resource'

Vue.use(Resource)


Vue.config.devtools = false
Vue.config.productionTip = false


import routes from './routes'
import Auth from './Auth'


document.addEventListener('DOMContentLoaded', () =>
{
	var Party = new Vue(
	{
		router: new Router({ routes }),
		template: '<router-view />',

		provide:
		{
			auth: Auth(),
		},
	})

	Party.$mount('#app')
})
