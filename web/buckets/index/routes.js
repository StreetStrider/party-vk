
import App  from 'App/App'
import Form from 'Form/Form'

export default
[
	{ path: '/', component: App },

	{ path: '/form', component: Form }
]
